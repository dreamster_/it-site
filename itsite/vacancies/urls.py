from django.conf.urls import url
from . import views

urlpatterns = [
	url(r'^$', views.index, name='index'),
	url(r'^vacancy/(?P<vacancy_id>[0-9]+)/$', views.vacancydetails, name='vacancydetails'),
	url(r'^company/(?P<company_id>[0-9]+)/$', views.companydetails, name='companydetails'),					
]