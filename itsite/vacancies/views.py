from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from vacancies.models import Vacancy, Company, Type

def index(request):
	vacancies = Vacancy.objects.all()
	context = {'vacancies_list': vacancies,}
	return render(request, 'vacancies/index.html', context)

def companydetails(request, company_id):
	company = get_object_or_404(Company, id=company_id)
	return HttpResponse(company)

def vacancydetails(request, vacancy_id):
	vacancy = get_object_or_404(Vacancy, id=vacancy_id)
	company = get_object_or_404(Company, id=vacancy.company_id)
	context = {'name': vacancy.vacancy_name, 'salary': vacancy.salary, 'company': company.company_name}
	return render(request, 'vacancies/vacancy.html', context)