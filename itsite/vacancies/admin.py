from django.contrib import admin
from vacancies.models import Vacancy, Type, Company

admin.site.register(Company)
admin.site.register(Type)
admin.site.register(Vacancy)
