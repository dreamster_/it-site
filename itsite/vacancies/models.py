from django.db import models

class Type(models.Model):
	type = models.CharField(max_length=100)

	def __str__(self):
		return self.type

class Company(models.Model):
	company_name = models.CharField(max_length=100)
	company_type = models.ForeignKey(Type, on_delete=models.CASCADE)

	def __str__(self):
		return self.company_name

class Vacancy(models.Model):
	vacancy_name = models.CharField(max_length=100)
	salary = models.IntegerField(default=0)
	company = models.ForeignKey(Company, on_delete=models.CASCADE)
	
	def __str__(self):
		return self.vacancy_name
		