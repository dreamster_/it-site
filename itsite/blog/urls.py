from django.urls import path
from django.views.generic.base import RedirectView
from . import views

urlpatterns = [
	path('page/<int:page_number>/', views.page, name='page'),
	path('post/<int:post_id>/', views.post, name='post'),
	path('', RedirectView.as_view(url='page/1/')),
]