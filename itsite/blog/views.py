from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from blog.models import Post


def page(request, page_number):
	start_post = (page_number - 1) * 5
	end_post = page_number * 5
	all_posts = Post.objects.all()
	page = all_posts[start_post:end_post]

	if (len(all_posts) % 5 == 0):
		number_of_pages = len(all_posts) // 5
	else:
		number_of_pages = len(all_posts) // 5 + 1

	pages = [i for i in range(1, number_of_pages+1)]

	for post in page:
		if (len(post.content) > 500):
			i = 500
			while (post.content[i] != '\n'):
				i -= 1
			post.content = post.content[:i]

	context = {'posts': page, 'pages': pages}
	return render(request, 'blog/page.html', context)


def post(request, post_id):
	post = get_object_or_404(Post, id=post_id)
	context = {'content': post.content}
	return render(request, 'blog/post.html', context)
	