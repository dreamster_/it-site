from django.db import models

class Post(models.Model):
	header = models.CharField(max_length=250)
	date = models.DateField('Publishing date')
	#author = models.CharField(max_length=50)
	content = models.TextField(max_length=10000)

	def __str__(self):
		return self.header
